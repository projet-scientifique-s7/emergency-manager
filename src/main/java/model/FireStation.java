package model;

import java.util.ArrayList;
import java.util.List;

public class FireStation {
    //attributes
    public int id;
    public String name;
    public Coord coordinates;
    public int human_capacity;
    public List<Truck> trucks = new ArrayList<Truck>();

    //constructor
    FireStation(int id, String name, float x, float y, int human_capacity){
        this.id = id;
        this.name = name;
        coordinates.x = x;
        coordinates.y = y;
        this.human_capacity = human_capacity;
    }

    //getters and setters
    public void addTruck(Truck truck){
        trucks.add(truck);
    }

    public Truck getTruck(int id){
        for(int i = 0; i <= trucks.size(); i++){
            if(trucks.get(i).id == id) return trucks.get(i);
        }
        return null;
    }

    public void setCoordinates(float x, float y) {
        this.coordinates.x = x;
        this.coordinates.y = y;
    }

    public void setZCoordinate(float z) {
        this.coordinates.z = z;
    }

    //functions
    public boolean canIntervene(Coord coord){
        return false;
    }

    public Truck selectTruckForFire(FirePredict firePredict){ return null; }
}
