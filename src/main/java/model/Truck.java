package model;

public class Truck {
    //attributes
    public int id;
    public int id_type;
    public int human_capacity;
    public State state;

    //constructor
    Truck(int id, int id_type, int human_capacity, State state){
        this.id = id;
        this.id_type = id_type;
        this.human_capacity = human_capacity;
        this.state = state;
    }

    //functions
    public void dispatch(){

    }
}
