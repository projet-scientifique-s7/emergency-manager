package model;

public enum State {
    IN_STATION,
    IN_COMING,
    ON_FIRE,
    COMING_BACK
}
