package model;

import org.json.JSONObject;

public class Sensor extends APIObject{
    //attributes
    public String id;
    public Coord coordinates;
    private int state;

    public Sensor(){

    }

    //constructor
    public Sensor(String id, float x, float y, int z){
        this.id = id;
        coordinates = new Coord(x, y, z);
        this.state = 0;
    }

    public Sensor(String id, Coord coord){
        this.id = id;
        this.coordinates = coord;
        this.state = 0;
    }

    //getters and setters
    public void setState(int _state){
        if(_state > 10) _state = 10;
        if(_state < 0) _state = 0;
        state = _state;
    }

    public void setCoordinates(float x, float y) {
        this.coordinates.x = x;
        this.coordinates.y = y;
    }

    public void setZCoordinate(int z) {
        this.coordinates.z = z;
    }

    //functions
    public void update(){

    }

    //TODO Ajouter un JSON parser
    public void ParceJSON(JSONObject jsonObject) {
        this.id = String.valueOf(jsonObject.getInt("id"));
        this.state = jsonObject.getInt("etat");
        Coord coord = new Coord();
        coord.ParceJSON(jsonObject.getJSONObject("coordonnees"));
        this.coordinates = coord;
    }

    public String getJSONString(){
        if (id != null){
            return "{\"id\": \"" + this.id + "\", \"etat\": \"" + this.state + "\", \"x\": \"" + this.coordinates.x  + "\", \"y\":\"" + this.coordinates.y + "\", \"z\": \""+ this.coordinates.z + "\" }";
        }else{
            return "{\"etat\": \"" + this.state + "\", \"x\": \"" + this.coordinates.x  + "\", \"y\":\"" + this.coordinates.y + "\", \"z\": \""+ this.coordinates.z + "\" }";

        }
    }

    public String getJSONStringToPUT(){
        return "{\"etat\": \"" + this.state + "\", \"date\":\"2022-01-03\" }";
    }

    public String toString(){
        return "id: " + this.id + ", state: " + this.state + ", coordonnees:" + this.coordinates;
    }


}
