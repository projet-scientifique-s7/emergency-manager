package model;

import java.util.ArrayList;
import java.util.List;

public class FirePredict {

    //attributes
    public int id;
    public Coord coordinates;
    public int intensity_predict;
    public List<Truck> interveningTrucks = new ArrayList<Truck>();

    //constructor
    FirePredict(int id, int intensity_predict, int x, int y){
        this.id = id;
        this.intensity_predict = intensity_predict;
        coordinates.x = x;
        coordinates.y = y;
    }

    //getters and setters
    public void addTruck(Truck truck){
        interveningTrucks.add(truck);
    }

    public Truck getTruck(int id){
        for(int i = 0; i <= interveningTrucks.size(); i++){
            if(interveningTrucks.get(i).id == id) return interveningTrucks.get(i);
        }
        return null;
    }

    public void setCoordinates(float x, float y) {
        this.coordinates.x = x;
        this.coordinates.y = y;
    }

    public void setZCoordinate(int z) {
        this.coordinates.z = z;
    }

    //functions
    public void update(){

    }
}
