package model;

import org.json.JSONObject;

public class Coord extends APIObject {
    //attributes
    public double x;
    public double y;
    public int z;


    public Coord(){

    }

    //constructor
    public Coord(double x, double y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void ParceJSON( JSONObject jsonObject) {
        this.x = jsonObject.getDouble("x");
        this.y = jsonObject.getDouble("y");
        this.z = jsonObject.getInt("z");

    }

    public String getJSONString(){
        return "{\"x\": \"" + this.x + "\", \"y\": \"" + this.y + "\", \"z\":\"" + this.z + "\"}";
    }

    public String toString(){
        return "x:" + this.x +" ,y:" + this.y + " ,z:" + z;
    }

    //TODO Ajouter un JSON parser
}
